# Team Members

Spencer DeBuf

# Idea 1

Tutorial: Convert ER Diagram to Relational Model

The goal of this tutorial is to teach the reader how to convert an ER (or EER) diagram to a Relational Model. We will address the proper procedures translating the model to properly represent relations, keys (foreign and primary), attributes, etc. 

We think the material learned in this tutorial is important because an efficient and effect database system starts with the fundamentals: design and architecture. A properly designed Relational Model will yield a more efficient and robust database to the user (and to the developers updating and maintaining the database).

# Idea 2

Tutorial: Convert Relational Schema to Normal Forms

The goal of this tutorial is to teach the reader how to convert a Relational Schema to one of the varying Normal Forms. We will address the benefits of each Normal Form, the complexity of modifying your schema to meet the requirements of each Normal Form, and the specific procedures for the conversion. 

A thorough knowledge of these Normal Forms yields a strong knowledge of the foundation of the theories involved in database design. As database projects grow larger in scale and complexity, problems arise with redundancy, efficiency, and durability. Understanding and applying the Normal Forms to your Relational Schema can help manage, minimize, or even eliminate these problems. 


# Idea 3

Tutorial: Querying

The goal of this tutorial is to teach the reader how to query databases to obtain relevant and purpose-specific information. We will outline the syntax for queries as well as some strategies for successfully obtaining data via complex requests. 

Creating and maintaining a database system can be a tedious process. The cost of such a system can be offset if the user has the ability to gather data that may be important and otherwise impractical to obtain. Learning the proper techniques for querying database systems is crucial to maximizing the benefits of a database system. 


