/* For every situation where student A likes student B, but student B likes a different student C, return the names and grades of A, B, and C. */

drop view if exists temp;

select a.name, a.grade, b.name, b.grade, c.name, c.grade from highschooler as a, likes as l1, highschooler as b, likes as l2, highschooler as c where a.id = l1.id1 and b.id = l1.id2 and b.id = l2.id1 and c.id = l2.id2 and not a.id = c.id;