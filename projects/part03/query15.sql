/* For each student A who likes a student B where the two are not friends, find if they have a friend C in common (who can introduce them!). For all such trios, return the name and grade of A, B, and C. */

drop view if exists likeslist;
drop view if exists mutual;

create view likeslist as select h1.id as l1, h2.id as l2 from highschooler as h1, likes, highschooler as h2 where (l1, l2) not in friend and (l2, l1) not in friend and h1.id = likes.id1 and h2.id = likes.id2;

create view mutual as select l1 as m1, h.id as m, l2 as m2 from likeslist, highschooler as h where 
((m1, m) in friend and (m2, m) in friend) or 
((m, m1) in friend and (m2, m) in friend) or 
((m1, m) in friend and (m, m2) in friend) or
((m, m1) in friend and (m, m2) in friend);

select h1.name, h1.grade, mm.name, mm.grade, h2.name, h2.grade from mutual as m, highschooler as h1, highschooler as h2, highschooler as mm where m.m1 = h1.id and m.m = mm.id and m.m2 = h2.id;