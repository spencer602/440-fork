/* Find the name and grade of all students who are liked by more than one other student. */

drop view if exists liked;

create view liked as select l2.name, l2.grade from highschooler as l1, likes as l, highschooler as l2 where l1.id = l.id1 and l2.id = l.id2 and (select count(*) from likes where l2.id = likes.id2) >= 2;

select distinct * from liked;