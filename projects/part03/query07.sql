/* Find the number of students who are either friends with Cassandra or are friends of friends of Cassandra. Do not count Cassandra, even though technically she is a friend of a friend. */

drop table if exists temp;
drop table if exists temp1;

create table temp (id int);
create table temp1 (id int);

insert into temp select distinct id1 from highschooler, friend where name = "Cassandra" and id2 = id;

insert into temp select distinct id2 from highschooler, friend where name = "Cassandra" and id1 = id;

insert into temp1 select distinct * from temp;

insert into temp1 select distinct f.id1 from temp as t, friend as f, highschooler as h where f.id2 = t.id and t.id = h.id and not h.name = "Cassandra" and f.id1 not in temp1;

insert into temp1 select distinct f.id2 from temp as t, friend as f, highschooler as h where f.id1 = t.id and t.id = h.id and not h.name = "Cassandra" and f.id2 not in temp1;

select count(*) from temp1;
