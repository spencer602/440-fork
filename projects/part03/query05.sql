/* Find those students for whom all of their friends are in different grades from themselves. Return the students' names and grades. */

create view temp5 as select h.id from highschooler as h, friend as f, highschooler as hf where h.id = f.id1 and f.id2 = hf.id and hf.grade = h.grade;

create view temp6 as select hf.id from highschooler as h, friend as f, highschooler as hf where h.id = f.id1 and f.id2 = hf.id and hf.grade = h.grade;

select highschooler.name, highschooler.grade from highschooler where highschooler.id not in temp5 and highschooler.id not in temp6;

