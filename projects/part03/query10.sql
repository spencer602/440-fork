/* For every student who likes someone 2 or more grades younger than themselves, return that student's name and grade, and the name and grade of the student they like.
 */

select h1.name, h1.grade, h2.name, h2.grade from highschooler as h1, likes , highschooler as h2 where h1.id = id1 and h2.id = id2 and h1.grade - h2.grade >= 2;

