/* For every situation where student A likes student B, but we have no information about whom B likes (that is, B does not appear as an ID1 in the Likes table), return A and B's names and grades. */

drop view if exists likesid;

create view likesid as select id1 as id from likes;

select h1.name, h1.grade, h2.name, h2.grade from highschooler as h1, likes as l, highschooler as h2 where l.id1 = h1.id and l.id2 not in likesid and l.id2 = h2.id;