/* For all cases where A is friends with B, and B is friends with C, add a new friendship for the pair A and C. Do not add duplicate friendships, friendships that already exist, or friendships with oneself.*/

create table temp (id1 int, id2 int);

insert into temp select f1.id2, f2.id2 from friend as f1, friend as f2 where f1.id1 = f2.id1 and (f1.id2, f2.id2) not in temp;

insert into temp select f1.id2, f2.id1 from friend as f1, friend as f2 where f1.id1 = f2.id2 and (f1.id2, f2.id1) not in temp;

insert into temp select f1.id1, f2.id2 from friend as f1, friend as f2 where f1.id2 = f2.id1 and (f1.id1, f2.id2) not in temp;

insert into temp select f1.id1, f2.id1 from friend as f1, friend as f2 where f1.id2 = f2.id2 and (f1.id1, f2.id1) not in temp;

delete from temp where id1 = id2;

delete from temp where id1 > id2;

insert into friend select id1, id2 from temp where (id1, id2) not in friend and (id2, id1) not in friend;

select * from friend order by id1;
