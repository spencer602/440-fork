/* If two students A and B are friends, and A likes B but not vice-versa, remove the Likes tuple */

drop table if exists temp;
drop view if exists temp;

create view temp as select plikes.id1 as cid1, plikes.id2 as cid2 from likes as plikes, friend as pfriend where (plikes.id1 = pfriend.id1 and plikes.id2 = pfriend.id2) or (plikes.id2 = pfriend.id1 and plikes.id1 = pfriend.id2);

delete from likes where (id1, id2) in (select cid1 as id1, cid2 as id2 from temp where not exists (select * from temp where cid1 = id2 and cid2 = id1));
