/* For every pair of students who both like each other, return the name and grade of both students. Include each pair only once, with the two names in alphabetical order. */

drop view if exists temp;
drop table if exists temp;

create view temp as select h1.id as h1, h2.id as h2 from highschooler as h1, likes as l1, likes as l2, highschooler as h2 where h1.id = l1.id1 and h2.id = l1.id2 and h2.id = l2.id1 and h1.id = l2.id2 and h1.name < h2.name;

select * from temp;

select hs1.name as h1n, hs2.name as h2n from temp, highschooler as hs1, highschooler as hs2 where h1 = hs1.id and h2 = hs2.id;

