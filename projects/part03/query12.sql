/* Find all students who do not appear in the Likes table (as a student who likes or is liked) and return their names and grades. Sort by grade, then by name within each grade. */

drop view if exists temp;
drop table if exists temp;

create view temp as select h.id from highschooler as h, likes as l where l.id1 = h.id or l.id2 = h.id;

delete from highschooler where highschooler.id in temp;

select grade, name from highschooler order by grade, name;

