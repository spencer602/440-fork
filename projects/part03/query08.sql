/* Find the name and grade of the student(s) with the greatest number of friends. */

drop view if exists temp;
drop view if exists temp1;
drop view if exists temp2;

create view temp as select id as id, count(*) as ct from highschooler, friend where id = id1 or id = id2 group by id;

create view temp2 as select max(ct) as m from temp;

create view temp1 as select name, grade from temp, highschooler, temp2 where highschooler.id = temp.id and ct = temp2.m;

select * from temp1;
