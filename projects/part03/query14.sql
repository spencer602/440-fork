/* Find names and grades of students who only have friends in the same grade. Return the result sorted by grade, then by name within each grade. */

drop view if exists excluded;
drop table if exists excluded;

create table excluded (id int);

insert into excluded select distinct h1.id from highschooler as h1, friend as f, highschooler as h2 where h1.id = f.id1 and f.id2 = h2.id and not h1.grade = h2.grade;

insert into excluded select distinct h2.id from highschooler as h1, friend as f, highschooler as h2 where h1.id = f.id1 and f.id2 = h2.id and not h1.grade = h2.grade and h2.id not in excluded;

select * from excluded;

delete from highschooler where highschooler.id in excluded;

select grade, name from highschooler order by grade, name;