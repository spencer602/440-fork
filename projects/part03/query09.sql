/* Find the names of all students who are friends with someone named Gabriel. */

drop table if exists temp;
drop view if exists temp;

create table name(name text);

insert into name select distinct f.name from highschooler as g, friend, highschooler f where id1 = g.id and g.name = 'Gabriel' and id2 = f.id;
insert into name select distinct f.name from highschooler as g, friend, highschooler f where id2 = g.id and g.name = 'Gabriel' and id1 = f.id and f.name not in name;

select * from name;