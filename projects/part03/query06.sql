/* What is the average number of friends per student? (Your result should be just one number.) */

create view temp as select id as id, count(*) as ct from highschooler, friend where id = id1 or id = id2 group by id;

select avg(ct) from temp;